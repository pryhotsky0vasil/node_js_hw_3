const { User } = require('../models/user.model');
const { createError } = require('../services/error.service');

const driverRoleMiddleware = async (req, res, next) => {
	const { id } = req.user;
	const { role } = await User.findById(id);
	if (role !== 'DRIVER') {
		next(createError(400, 'Access denied'));
	}
	next();
};
 
module.exports = driverRoleMiddleware;
