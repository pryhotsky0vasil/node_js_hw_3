const { createError } = require('../services/error.service');
const { verifyJWT } = require('../services/JWT.service');

const authMiddleware = (req, res, next) => {
	const { authorization } = req.headers;
	if (!authorization) {
		next(createError(400, 'Please, provide authorization header.'));
	}

	const [, token] = req.headers.authorization.split(' ');

	if (!token) {
		next(createError(400, 'Access denied. No token provided'));
	}
	req.user = verifyJWT(token);
	next(); 
};

module.exports = authMiddleware;
