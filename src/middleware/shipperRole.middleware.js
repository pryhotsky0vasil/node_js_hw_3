const { User } = require('../models/user.model');
const { createError } = require('../services/error.service');

const shipperRoleMiddleware = async (req, res, next) => {
	const { id } = req.user;
	const { role } = await User.findById(id);
	if (role !== 'SHIPPER') {
		next(createError(400, 'Access denied'));
	}
	next();
};

module.exports = shipperRoleMiddleware;
