const logHandler = (load, message) => load.updateOne({ $push: { logs: { message: message } } });

const changeState = (state) => {
	const states = [
		'En route to Pick Up',
		'Arrived to Pick Up',
		'En route to delivery',
		'Arrived to delivery',
	];
	const nextState = states[states.indexOf(state) + 1];
	return nextState;
};

module.exports = { logHandler, changeState };
