const { hash, compare } = require('bcryptjs');
const { createTransport } = require('nodemailer');
const { generate } = require('generate-password');
const { Credentials } = require('../models/credentials.model');
const { MAIL_USER, MAIL_PASSWORD } = process.env;

const hashPassword = (password) => hash(password, 10);

const comparePassword = (firstPassword, secondPassword) =>
	compare(String(firstPassword), String(secondPassword));

const forgotPassword = async (req, next) => {
	const { email } = req.body;
	const credential = await Credentials.findOne({ email });
	const transporter = createTransport({
		host: 'smtp.gmail.com',
		port: 587,
		secure: false,
		auth: {
			user: MAIL_USER,
			pass: MAIL_PASSWORD,
		},
	});
	const newPassword = generate({ length: 25, numbers: true });

	await credential.updateOne({ password: hashPassword(newPassword) });

	await transporter.sendMail({
		from: `"NEW PASSWORD" ${MAIL_USER}`,
		to: email,
		subject: 'YOUR NEW PASSWORD',
		html: `<b>Hello! Your new password is:\n<em style="color:orange">${newPassword}</em></p>`,
	});
};

module.exports = { hashPassword, comparePassword, forgotPassword };
