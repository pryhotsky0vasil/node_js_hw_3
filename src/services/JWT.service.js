const { sign, verify } = require('jsonwebtoken');
const { SECRET_KEY } = process.env;

const generateJWT = async (id) => sign({id}, SECRET_KEY);

const verifyJWT = (token) => verify(token, SECRET_KEY);

module.exports = { generateJWT, verifyJWT };
 