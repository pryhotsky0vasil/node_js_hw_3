const { Credentials } = require('../models/credentials.model');
const { User } = require('../models/user.model');
const { createError } = require('./error.service');
const { generateJWT } = require('./JWT.service');
const { comparePassword } = require('./password.service');

const login = async (req, next) => {
	const { email, password } = req.body;
	const user = await User.findOne({ email });
	if (!user) {
		next(createError(400, 'Please provide email and password'));
	}
	const credentials = await Credentials.findOne({ email });
	const isPasswordMatch = await comparePassword(password, credentials.password);

	if (!isPasswordMatch) {
		next(createError(400, 'Password is incorrect'));
	}
	return generateJWT(credentials.userId);
};

module.exports = { login };
