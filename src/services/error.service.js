const createError = (status, message) => {
	const err = new Error();
	err.status = status;
	err.message = message;
	return err;
};

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

const errorHandler = (err, req, res, next) =>
	res.status(err.status || 500).send({ message: err.message || 'Internal server error' });

module.exports = { createError, errorHandler, asyncWrapper };
