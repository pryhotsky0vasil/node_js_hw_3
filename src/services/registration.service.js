const { hashPassword } = require('./password.service');
const { Credentials } = require('../models/credentials.model');
const { User } = require('../models/user.model');

const createCredentials = async ({ email, password, userId }) =>
	await new Credentials({ email, password: await hashPassword(password), userId }).save();

const createUser = async ({ email, role }) => await new User({ email, role }).save();

const registration = async ({ email, password, role }) => {
	const user = await createUser({ email, role });
	createCredentials({ email, password, userId: user._id });
};

module.exports = registration;
