const { connect } = require('mongoose');

const { DB_USER, DB_PASSWORD } = process.env;

const URI = `mongodb+srv://${DB_USER}:${DB_PASSWORD}@cluster0.xgm48oz.mongodb.net/?retryWrites=true&w=majority`;

const connectDB = () => connect(URI);

module.exports = {connectDB};
