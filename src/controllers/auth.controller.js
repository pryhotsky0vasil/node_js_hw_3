const registration = require('../services/registration.service');
const { registrationCredentialJoiSchema } = require('../models/registrationCredentials.model');
const { login } = require('../services/login.service');
const { createError } = require('../services/error.service');
const { forgotPassword } = require('../services/password.service');
const e = require('cors');

const createUser = async (req, res, next) => {
	const registrationData = req.body;
	registrationCredentialJoiSchema.validateAsync(registrationData);

	await registration(registrationData);
	return res.json({ message: 'Profile created successfully' });
};

const loginUser = async (req, res, next) => {
	const token = await login(req, next);
	if (!token) {
		next(createError(400, 'Not authorized'));
	}
	return res.json({ jwt_token: token });
};

const resetPassword = async (req, res, next) => {
	const { email } = req.body;
	if (!email) {
		next(createError(400, 'Provide email'));
	}
	
	await forgotPassword(req, next);

	return res.json({ message: 'New password sent to your email address' });
};

module.exports = { createUser, loginUser, resetPassword };
