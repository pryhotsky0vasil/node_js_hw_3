const { User } = require('../models/user.model');
const { Credentials } = require('../models/credentials.model');
const { hashPassword, comparePassword } = require('../services/password.service');
const { createError } = require('../services/error.service');

const getProfileInfo = async (req, res, next) => {
	const { id } = req.user;
	const user = await User.findById(id);

	return res.json(user);
};

const deleteProfile = async (req, res, next) => {
	const { id } = req.user;
	await User.findByAndDelete({ id });
	await Credentials.findOneAndDelete({ id });

	return res.json({ message: 'Profile deleted successfully' });
};

const changeProfilePassword = async (req, res, next) => {
	const {
		user: { id },
		body: { oldPassword, newPassword },
	} = req;
	const user = await Credentials.findOne({ id });

	if (!(await comparePassword(oldPassword, user.password))) {
		next(createError(400, "Old password doesn't match"));
	}

	if (oldPassword === newPassword) {
		next(createError(400, 'Passwords must not match'));
	}

	const newHashedPassword = await hashPassword(newPassword);

	await user.updateOne({ password: newHashedPassword });
	return res.json({ message: 'Password changed successfully' });
};

module.exports = { getProfileInfo, deleteProfile, changeProfilePassword };
