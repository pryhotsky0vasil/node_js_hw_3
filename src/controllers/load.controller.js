const { Load, loadJoiSchema } = require('../models/load.model');
const { Truck } = require('../models/truck.model');
const { User } = require('../models/user.model');
const { createError } = require('../services/error.service');
const { logHandler, changeState } = require('../services/load.service');

const getLoads = async (req, res, next) => {
	const {
		user: { id },
		params: { status, limit = 0, offset = 0 },
	} = req;
	const { role } = await User.findById(id);

	let foundedLoads = null;

	switch (role) {
		case 'SHIPPER':
			{
				foundedLoads = await Load.find({ created_by: id }).skip(offset).limit(limit);
				console.log(role);
			}
			break;
		case 'DRIVER':
			{
				foundedLoads = await Load.find({ assigned_to: id }).skip(offset).limit(limit);
				console.log(role);
			}
			break;
		default:
			{
			}
			break;
	}
	const loads = status ? foundedLoads.filter((load) => load.status === status) : foundedLoads;

	return res.json({ loads });
};

const addLoad = async (req, res, next) => {
	const {
		user: { id: userId },
		body: { name, payload, pickup_address, delivery_address, dimensions },
	} = req;

	loadJoiSchema.validate(name, payload, pickup_address, delivery_address, dimensions);

	const load = new Load({
		created_by: userId,
		name,
		payload,
		pickup_address,
		delivery_address,
		dimensions,
	});

	await load.save();
	await logHandler(load, 'Load created');

	return res.json({ message: 'Load created successfully' });
};

const getActiveLoad = async (req, res, next) => {
	const { id } = req.user;
	const activeLoad = await Load.find({ assigned_to: id, status: 'ASSIGNED' });
	if (!activeLoad) {
		next(createError(400, 'Load not found'));
	} else {
		return res.json({ load: activeLoad });
	}
};

const iterateNextLoadState = async (req, res, next) => {
	const { id } = req.user;
	const load = await Load.findOne({ assigned_to: id, status: 'ASSIGNED' });
	if (load) {
		const nextState = changeState(load.state);
		await load.updateOne({ state: nextState });
		await logHandler(load, `Load state changed to '${nextState}'`);
		console.log(load);
		if (nextState === 'Arrived to delivery') {
			const truck = await Truck.findOne({ shipperId: load.created_by });
			await load.updateOne({ status: 'SHIPPED' });
			await truck.updateOne({ status: 'IS', shipperId: null });
		}

		return res.json({ message: `Load state changed to '${nextState}'` });
	} else {
		next(createError(400, 'Load not found'));
	}
};

const getLoad = async (req, res, next) => {
	const { id } = req.params;
	const load = await Load.findById(id);

	if (!load) {
		next(createError(400, 'Load not found'));
	}

	return res.json({ load });
};

const updateLoad = async (req, res, next) => {
	const {
		params: { id },
		body,
	} = req;
	const load = await Load.findById(id);
	if (!load) {
		next(createError(400, 'Load not found '));
	}
	if (load.status === 'NEW') {
		await load.updateOne(body);
		await logHandler(load, 'Load updated');

		return res.json({ message: 'Load details changed successfully' });
	} else {
		next(createError(400, 'Cannot update'));
	}
};

const deleteLoad = async (req, res, next) => {
	const { id } = req.params;
	const load = await Load.findById(id);
	if (!load) {
		next(createError(400, 'Load not found '));
	}
	if (load.status === 'NEW') {
		await load.deleteOne();
		return res.json({ message: 'Load deleted successfully' });
	} else {
		next(createError(400, 'Cannot delete'));
	}
};

const postLoad = async (req, res, next) => {
	const { id } = req.params;
	const load = await Load.findById(id);
	if (!load) {
		next(createError(400, 'Load not found'));
	}
	if (load.status === 'ASSIGNED') {
		next(createError(400, 'Load is already assigned'));
	}

	const {
		payload,
		dimensions: { width, height, length },
	} = load;

	await load.updateOne({ status: 'POSTED' });
	await logHandler(load, 'Load posted');

	const truck = await Truck.findOne({
		status: 'IS',
		assigned_to: { $ne: null },
		capacity: { $gt: payload },
		length: { $gt: length },
		width: { $gt: width },
		height: { $gt: height },
	});

	if (truck) {
		await truck.updateOne({ status: 'OL', shipperId: req.user.id });
		await load.updateOne({
			status: 'ASSIGNED',
			state: 'En route to Pick Up',
			assigned_to: truck.created_by,
		});
		await logHandler(load, 'Driver was found');
	} else {
		await load.updateOne({ status: 'NEW' });
		await logHandler(load, 'Driver was not found');
	}

	return res.json({ message: 'Load posted successfully', driver_found: !!truck });
};

const getSHippingInfo = async (req, res, next) => {
	const { id } = req.params;
	const load = await Load.findById(id);
	if (!load || load.status !== 'ASSIGNED') {
		next(createError(400, 'Load not found '));
	}
	const { shipperId, ...truckInfo } = await Truck.findOne({ shipperId: load.created_by });

	return res.json({ load, truck: truckInfo });
};

module.exports = {
	getLoads,
	addLoad,
	getActiveLoad,
	iterateNextLoadState,
	getLoad,
	updateLoad,
	deleteLoad,
	postLoad,
	getSHippingInfo,
};
