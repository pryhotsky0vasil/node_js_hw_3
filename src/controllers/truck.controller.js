const { getTruckParams } = require('../../trucks_DB');
const { Truck, truckJoiSchema } = require('../models/truck.model');
const { createError } = require('../services/error.service');

const getTrucks = async (req, res, next) => {
	const { id: userId } = req.user;
	const trucks = await Truck.find({ created_by: userId });
	if (!trucks) {
		next(createError(404, 'Trucks are not found'));
	} else {
		return res.json({ trucks });
	}
};

const addTrack = async (req, res, next) => {
	const {
		user: { id: userId },
		body: { type },
	} = req;

	const {
		capacity,
		dimensions: { width, height, length },
	} = getTruckParams(type);

	truckJoiSchema.validate(type);

	await new Truck({ created_by: userId, type, capacity, width, height, length }).save();

	return res.json({ message: 'Truck created successfully' });
};

const getTruck = async (req, res, next) => {
	const { id } = req.params;
	const truck = await Truck.findById(id);
	console.log(truck);

	if (!truck) {
		next(createError(404, `Truck id:${id} is not found`));
	} else {
		return res.json({ truck });
	}
};

const updateTruck = async (req, res, next) => {
	const {
		body: { type },
		params: { id },
	} = req;

	const truck = await Truck.findById(id);
	console.log(truck);
	if (truck.status === 'OL') {
		next(createError(400, 'Truck is on a load'));
	}

	await truck.update({ type });

	return res.json({ message: 'Truck details changed successfully' });
};

const deleteTruck = async (req, res, next) => {
	const {
		params: { id },
		user: { id: userId },
	} = req.params;
	const truck = Truck.findById(id);

	if (!truck) {
		next(createError(400, `Truck ${id} is not found`));
	}
	if (truck.assigned_to === userId) {
		next(createError(400, `Truck ${id} is already assigned`));
	}

	await truck.deleteOne();

	return res.json({ message: `Truck id:${id}deleted successfully` });
};

const assignTruck = async (req, res, next) => {
	const {
		params: { id },
		user: { id: userId },
	} = req;
	const isAssigned = await Truck.findOne({ assigned_to: userId });
	if (isAssigned) {
		next(createError(400, 'Driver has assigned car'));
	} else {
		await Truck.findByIdAndUpdate(id, { assigned_to: userId });
		return res.json({ message: 'Truck assigned successfully' });
	}
};

module.exports = {
	getTrucks,
	addTrack,
	getTruck,
	updateTruck,
	deleteTruck,
	assignTruck,
};
