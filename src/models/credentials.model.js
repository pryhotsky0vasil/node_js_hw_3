const { model, Schema } = require('mongoose');

const Credentials = model(
  'Credentials',
  Schema({
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    userId: { type: Schema.Types.ObjectId, required: true, unique: true },
  }),
);

module.exports = { Credentials };
