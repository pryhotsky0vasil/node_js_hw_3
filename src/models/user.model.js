const Joi = require('joi');
const moment = require('moment');
const { model, Schema } = require('mongoose');

const userJoiSchema = Joi.object({
	email: Joi.string().email().required(),
	role: Joi.string()
		.valid(...['DRIVER', 'SHIPPER'])
		.required(),
});

const User = model(
	'User',
	Schema({
		email: { type: String, required: true, unique: true },
		role: { type: String, required: true, enum: ['DRIVER', 'SHIPPER'] },
		created_date: { type: Schema.Types.Date, default: moment().local() },
	}),
);

module.exports = { User, userJoiSchema };
