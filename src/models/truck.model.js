const Joi = require('joi');
const moment = require('moment');
const { model, Schema } = require('mongoose');

const truckJoiSchema = Joi.object({
	type: Joi.string()
		.required()
		.valid(...['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']),
});

const Truck = model(
	'Truck',
	Schema({
		created_by: { type: Schema.Types.ObjectId },
		assigned_to: { type: Schema.Types.ObjectId, default: null },
		type: {
			type: String,
			required: true,
			enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
		},
		status: {
			type: String,
			enum: ['OL', 'IS'],
			default: 'IS',
		},
		capacity: { type: Number },
		width: { type: Number },
		length: { type: Number },
		height: { type: Number },
		shipperId: { type: Schema.Types.ObjectId },
		created_date: {
			type: Schema.Types.Date,
			default: moment().local(),
		},
	}),
);

module.exports = { Truck, truckJoiSchema };
