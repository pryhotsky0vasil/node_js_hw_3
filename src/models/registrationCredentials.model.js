const Joi = require('joi');

const registrationCredentialJoiSchema = Joi.object({
	email: Joi.string().email().required(),
	password: Joi.string().alphanum().required(),
	role: Joi.string().required(),
});

module.exports = { registrationCredentialJoiSchema };
