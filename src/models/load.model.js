const { string } = require('joi');
const Joi = require('joi');
const moment = require('moment');
const { model, Schema } = require('mongoose');

const loadJoiSchema = Joi.object({
	name: Joi.string().required(),
	payload: Joi.number().required(),
	pickup_address: Joi.string().min(10).required(),
	delivery_address: Joi.string().min(10).required(),
	dimensions: {
		width: Joi.number().required(),
		length: Joi.number().required(),
		height: Joi.number().required(),
	},
});

const Load = model(
	'Load',
	Schema({
		created_by: { type: Schema.Types.ObjectId },
		assigned_to: { type: Schema.Types.ObjectId, default: null },
		status: {
			type: String,
			enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
			default: 'NEW',
		},
		state: {
			type: String,
			enum: [
				'En route to Pick Up',
				'Arrived to Pick Up',
				'En route to delivery',
				'Arrived to delivery ',
			],
			default: 'En route to Pick Up',
		},
		name: {
			type: String,
			required: true,
		},
		payload: {
			type: Number,
			required: true,
		},
		pickup_address: {
			type: String,
			required: true,
		},
		delivery_address: {
			type: String,
			required: true,
		},
		dimensions: {
			width: { type: Number, required: true },
			length: { type: Number, required: true },
			height: { type: Number, required: true },
		},
		logs: [
			{
				message: { type: String },
				time: { type: Schema.Types.Date, default: moment().local() },
				_id: false,
			},
		],
		created_date: { type: Schema.Types.Date, default: moment().local() },
	}),
);

module.exports = { Load, loadJoiSchema };
