const express = require('express');
const authMiddleware = require('../middleware/auth.middleware');
const driverRoleMiddleware = require('../middleware/driverRole.middleware');

const authRouter = require('./auth.router');
const loadRouter = require('./load.router');
const truckRouter = require('./truck.router');
const userRouter = require('./user.router');

const router = express.Router();

router.use('/auth', authRouter);
router.use('/users', authMiddleware, userRouter);
router.use('/loads', authMiddleware, loadRouter);
router.use('/trucks', authMiddleware, driverRoleMiddleware, truckRouter);

module.exports = router;
