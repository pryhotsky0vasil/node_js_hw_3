const express = require('express');
const {
	getLoads,
	addLoad,
	getActiveLoad,
	iterateNextLoadState,
	getLoad,
	updateLoad,
	deleteLoad,
	postLoad,
	getSHippingInfo,
} = require('../controllers/load.controller');
const authMiddleware = require('../middleware/auth.middleware');
const driverRoleMiddleware = require('../middleware/driverRole.middleware');
const shipperRoleMiddleware = require('../middleware/shipperRole.middleware');
const { asyncWrapper } = require('../services/error.service');

const router = express.Router();

router.get('/', authMiddleware, asyncWrapper(getLoads));
router.post('/', authMiddleware, shipperRoleMiddleware, asyncWrapper(addLoad));
router.get('/active', authMiddleware, driverRoleMiddleware, asyncWrapper(getActiveLoad));
router.patch(
	'/active/state',
	authMiddleware,
	driverRoleMiddleware,
	asyncWrapper(iterateNextLoadState),
);
router.get('/:id', authMiddleware, asyncWrapper(getLoad));
router.put('/:id', authMiddleware, shipperRoleMiddleware, asyncWrapper(updateLoad));
router.delete('/:id', authMiddleware, shipperRoleMiddleware, asyncWrapper(deleteLoad));
router.post('/:id/post', authMiddleware, shipperRoleMiddleware, asyncWrapper(postLoad));
router.get(
	'/:id/shipping_info',
	authMiddleware,
	shipperRoleMiddleware,
	asyncWrapper(getSHippingInfo),
);

module.exports = router;
