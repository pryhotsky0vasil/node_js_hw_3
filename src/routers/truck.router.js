const express = require('express');
const {
  getTrucks,
  addTrack,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/truck.controller');
const authMiddleware = require('../middleware/auth.middleware');
const { asyncWrapper } = require('../services/error.service');

const router = express.Router();

router.get('/', authMiddleware, asyncWrapper(getTrucks));
router.post('/', authMiddleware, asyncWrapper(addTrack));
router.get('/:id', authMiddleware, asyncWrapper(getTruck));
router.put('/:id', authMiddleware, asyncWrapper(updateTruck));
router.delete('/:id', authMiddleware, asyncWrapper(deleteTruck));
router.post('/:id/assign', authMiddleware, asyncWrapper(assignTruck));

module.exports = router;
