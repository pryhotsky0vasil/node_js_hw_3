const express = require('express');
const { createUser, loginUser, resetPassword } = require('../controllers/auth.controller');
const { asyncWrapper } = require('../services/error.service');

const router = express.Router();

router.post('/register', asyncWrapper(createUser));
router.post('/login', asyncWrapper(loginUser));
router.post('/forgot_password', asyncWrapper(resetPassword));

module.exports = router;
