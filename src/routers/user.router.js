const express = require('express');
const {
	getProfileInfo,
	deleteProfile,
	changeProfilePassword,
} = require('../controllers/user.controller');
const { asyncWrapper } = require('../services/error.service');

const router = express.Router();

router.get('/me', asyncWrapper(getProfileInfo));
router.delete('/me', asyncWrapper(deleteProfile));
router.patch('/me/password', asyncWrapper(changeProfilePassword));

module.exports = router;
