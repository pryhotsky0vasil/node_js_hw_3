const SPRINTER = {
	dimensions: {
		length: 300,
		width: 250,
		height: 170,
	},
	capacity: 1700,
};
const SMALL_STRAIGHT = {
	dimensions: {
		length: 500,
		width: 250,
		height: 170,
	},
	capacity: 2500,
};

const LARGE_STRAIGHT = {
	dimensions: {
		width: 350,
		length: 700,
		height: 200,
	},
	capacity: 4000,
};

const getTruckParams = (type) => {
	switch (type) {
		case 'SPRINTER':
			return SPRINTER;
		case 'SMALL STRAIGHT':
			return SMALL_STRAIGHT;
		case 'LARGE STRAIGHT':
			return LARGE_STRAIGHT;
	}
};

module.exports = { SPRINTER, SMALL_STRAIGHT, LARGE_STRAIGHT, getTruckParams };
