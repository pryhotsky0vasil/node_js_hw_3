const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

require('dotenv').config();

const { PORT } = process.env;

const appRouters = require('./src/routers/index');
const { connectDB } = require('./src/services/db.service');
const { errorHandler } = require('./src/services/error.service');

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api', appRouters);

app.use(errorHandler);

app.listen(PORT, () => {
	connectDB();
	console.log(`Server has been started on port ${PORT}`);
});
